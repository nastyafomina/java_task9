public class Check implements ICheckAge {
    @Override
    public boolean checkAge(Human human1, Human human2, Human human3, int maxAge) {
        if(human1.getAge() < maxAge && human2.getAge() < maxAge && human3.getAge() < maxAge){
            return true;
        }
        return false;
    }
}

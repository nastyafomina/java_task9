import java.util.function.BiPredicate;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.UnaryOperator;

public class LambdaDemo {
    public static final Function<String, Integer> length = (String string) -> string.length();

    /* Филиппов А.В. 24.06.2020 Комментарий не удалять.
     Не работает! См. тест
    */
    public static final Function<String, Character> firstSymbol = (String string) -> {
        if(string.isEmpty()) { return null; }
        return string.charAt(0);
    };
    public static final Predicate<String> hasNotSpace = (String string) -> !string.contains(" ");

    /* Филиппов А.В. 24.06.2020 Комментарий не удалять.
     Не работает! См. тест. Пустых слов не бывает.
    */
    public static final Function<String, Integer> countOfWords = (String string) -> {
        int count = 0;
        String[] strings = string.split(",");
        for (String str : strings){
            if(!str.isEmpty()){ count++;}
        }
        return count;
    };
    public static final Function<Human, Integer> age = (Human human) -> human.getAge();

    /* Филиппов А.В. 24.06.2020 Комментарий не удалять.
     Есть BiPredicate
    */
    public static final BiPredicate<Human, Human> sameSurnames = (Human human1, Human human2) -> human1.getSurname().equals(human2.getSurname());
    public static final Function<Human, String> fullName = (Human human) -> human.getSurname()+" "+human.getName()+" "+human.getPatronymic();
    public static final UnaryOperator<Human> makeOlder = (Human human) -> new Human(human.getSurname(), human.getName(), human.getPatronymic(), human.getAge()+1, human.getSex());
    public static final ICheckAge allYoungerAge = (Human human1, Human human2, Human human3, int maxAge) -> human1.getAge()<maxAge && human2.getAge()<maxAge && human3.getAge()<maxAge;
}
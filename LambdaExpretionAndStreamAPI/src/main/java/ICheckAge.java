/* Филиппов А.В. 24.06.2020 Комментарий не удалять.
Приделал аннотацию.
*/
// спасибо ☺
@FunctionalInterface
public interface ICheckAge {
    boolean checkAge(Human human1, Human human2, Human human3, int maxAge);
}

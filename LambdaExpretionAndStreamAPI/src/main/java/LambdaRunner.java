import java.util.function.BiPredicate;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.UnaryOperator;

public class LambdaRunner {
    /* Филиппов А.В. 24.06.2020 Комментарий не удалять.
     В задании просили минимальное количество ранеров.
     Первые два можно точно объединить.
    */
    public static Object useFunctionString(Function<String, Object> lambda, String string){ return lambda.apply(string); }
    public static Object useFunctionHuman(Function<Human, Object> lambda, Human human){ return lambda.apply(human); }
    public static Boolean usePredicate(Predicate<String> lambda, String string){ return !lambda.test(string); }
    public static Boolean useBiPredicate(BiPredicate<Human, Human> lambda, Human human1, Human human2){
        return lambda.test(human1, human2); }
    public static Human useUnaryOperator(UnaryOperator<Human> lambda, Human human){ return lambda.apply(human); }
    public static Boolean useICheckAge(ICheckAge lambda, Human human1, Human human2, Human human3, int maxAge){
        return lambda.checkAge(human1, human2, human3, maxAge); }

}

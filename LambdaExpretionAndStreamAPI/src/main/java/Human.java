import java.util.*;

public class Human {
    private String surname;
    private String name;
    private String patronymic;
    private int age;
    private String sex;

    public Human(){}
    public Human(String surname, String name, String patronymic, int age, String sex) {
        this.surname = surname;
        this.name = name;
        this.patronymic = patronymic;
        this.age = age;
        this.sex = sex;
    }
    public String getName() { return name; }
    public String getSurname() {
        return surname;
    }
    public String getPatronymic() {
        return patronymic;
    }
    public int getAge() { return age; }
    public String getSex() { return sex; }

    public void setName(String name) {
        this.name = name;
    }
    public void setSurname(String surname) { this.surname = surname; }
    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }
    public void setAge(int age) {
        this.age = age;
    }
    public void setSex(String sex) {
        if (sex.equals("man") || sex.equals("woman")) {
            this.sex = sex;
        } else{
            throw new IllegalArgumentException();
        }
    }

    @Override
    public String toString(){
        return "Surname: " + surname + "\n" +
                "Name: " + name + "\n" +
                "Patronymic: " + patronymic + "\n" +
                "Age: " + age + " years old\n" +
                "Sex: " + sex + "\n";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        return age == human.age &&
                Objects.equals(name, human.name) &&
                Objects.equals(surname, human.surname) &&
                Objects.equals(patronymic, human.patronymic);
    }
    @Override
    public int hashCode() {
        return Objects.hash(name, surname, patronymic, age, sex);
    }
}

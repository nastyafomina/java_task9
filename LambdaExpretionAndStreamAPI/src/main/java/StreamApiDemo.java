import java.util.*;
import java.util.function.Function;
import java.util.function.UnaryOperator;
import java.util.stream.Collectors;

public class StreamApiDemo extends LambdaDemo {
    public static final UnaryOperator<List<Object>> deleteNull =
            (List<Object> objects) -> objects.stream().filter(Objects::nonNull).collect(Collectors.toList());
    public static final Function<Set<Integer>, Integer> countOfPositiveNumbers =
            (Set<Integer> set) -> (int) set.stream().filter(n -> n > 0).count();
    public static final UnaryOperator<List<Object>> getThreeLastElements = (List<Object> objects) -> {
        if(objects.size()>=3) {
            return objects.stream().skip(objects.size() - 3).collect(Collectors.toList());
        }
        return null;
    };
    public static final Function<List<Integer>, Integer> getFirstEvenNumber =
            (List<Integer> list) -> list.stream().filter(n -> n % 2 == 0).findFirst().orElse(null);
    public static final Function<Integer[], List<Integer>> getListOfSquaresWithoutRepetitions =
            (Integer[] array) -> Arrays.stream(array).map(n -> n*n).distinct().collect(Collectors.toList());
    /* Филиппов А.В. 24.06.2020 Комментарий не удалять.
     Улучшил.
    */
    public static final UnaryOperator<List<String>> getListOfStringWithoutEmpty =
            (List<String> list) -> list.stream().filter(s -> !s.equals("")).sorted(Comparator.naturalOrder()).collect(Collectors.toList());
    /* Филиппов А.В. 24.06.2020 Комментарий не удалять.
     Улучшил.
    */
    public static final Function<Set<String>, List<String>> setToListOfStrings =
            (Set<String> set) -> set.stream().sorted(Comparator.reverseOrder()).collect(Collectors.toList());
    /* Филиппов А.В. 24.06.2020 Комментарий не удалять.
     Улучшил.
    */
    public static final Function<Set<Integer>, Integer> getSumOfSquares = (Set<Integer> set) -> set.stream().mapToInt(n -> n*n).sum();

    /* Филиппов А.В. 24.06.2020 Комментарий не удалять.
     Улучшил.
    */
    public static final Function<Collection<Human>, Integer> getMaxAge =
            (Collection<Human> humans) -> humans.stream().map(Human::getAge).max(Comparator.naturalOrder()).orElse(null);

    /* Филиппов А.В. 24.06.2020 Комментарий не удалять.
     Не работает! Вы сортируете поток два раза. Вторая сортировка не учитывает первую.
     Нужен хитрый компоратор по двум полям сразу.
    */
    public static final UnaryOperator<Collection<Human>> sortHumansBySexAndAge =
            (Collection<Human> humans) -> humans.stream().sorted(new Comparator<Human>() {
                @Override
                public int compare(Human o1, Human o2) {
                    if(o1.getSex().compareTo(o2.getSex()) == 0) {
                        return o1.getAge() - o2.getAge();
                    }
                    return o1.getSex().compareTo(o2.getSex());
                }
            }).collect(Collectors.toList());
}

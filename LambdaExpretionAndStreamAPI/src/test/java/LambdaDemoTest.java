import org.junit.Test;
import static org.junit.Assert.*;

public class LambdaDemoTest {
    @Test
    public void test(){
        LambdaDemo lambdaDemo = new LambdaDemo();
        assertEquals(Integer.valueOf(5), LambdaDemo.length.apply("abcde"));
        assertEquals(Character.valueOf('a'), LambdaDemo.firstSymbol.apply("abcde"));
        assertTrue(LambdaDemo.hasNotSpace.test("abcde"));
        assertFalse(LambdaDemo.hasNotSpace.test("ab c de"));
        assertEquals(Integer.valueOf(3), LambdaDemo.countOfWords.apply("ab,c,de"));
        assertEquals(Integer.valueOf(42), LambdaDemo.age.apply(new Human("", "", "", 42, "")));
        assertTrue(LambdaDemo.sameSurnames.test(new Human("Ivanov", "", "", 0, ""),
                new Human("Ivanov", "", "", 0, "")));
        assertFalse(LambdaDemo.sameSurnames.test(new Human("Ivanov", "", "", 0, ""),
                new Human("Petrov", "", "", 0, "")));
        assertEquals("Ivanov Ivan Ivanovich", lambdaDemo.fullName.apply(new Human("Ivanov", "Ivan", "Ivanovich", 42, "")));
        assertEquals(43, (lambdaDemo.makeOlder.apply(new Human("Ivsnov", "Ivan", "Ivanovich", 42, "")).getAge()));
        assertTrue(lambdaDemo.allYoungerAge.checkAge(new Human("", "", "", 22, ""),
                new Human("", "", "", 33, ""), new Human("", "", "", 44, ""), 55));
        assertFalse(lambdaDemo.allYoungerAge.checkAge(new Human("", "", "", 22, ""),
                new Human("", "", "", 33, ""), new Human("", "", "", 44, ""), 11));
    }

    @Test
    public void test2() {
        assertEquals(null, LambdaDemo.firstSymbol.apply(""));
    }

    @Test
    public void test3() {
        assertEquals(Integer.valueOf(2), LambdaDemo.countOfWords.apply("ab,,de"));
    }
}
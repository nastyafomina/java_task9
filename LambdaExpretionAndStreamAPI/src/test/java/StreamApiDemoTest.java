import org.junit.Test;
import java.util.*;
import static org.junit.Assert.*;

public class StreamApiDemoTest {
    @Test
    public void testDeleteNull(){
        StreamApiDemo streamApiDemo = new StreamApiDemo();

        Human human1 =new Human();
        Human human2 =new Human();
        Student student = new Student();
        Object object = new Object();

        List<Object> objects = new ArrayList<>();
        objects.add(human1);
        objects.add(null);
        objects.add(human2);
        objects.add(null);
        objects.add(student);
        objects.add(object);

        List<Object> listExpected = new ArrayList<>();
        listExpected.add(human1);
        listExpected.add(human2);
        listExpected.add(student);
        listExpected.add(object);

        assertEquals(listExpected, streamApiDemo.deleteNull.apply(objects));
    }

    @Test
    public void testCountOfPositiveNumbers(){
        StreamApiDemo streamApiDemo = new StreamApiDemo();

        Set<Integer> set = new HashSet<>();
        set.add(1);
        set.add(-1);
        set.add(2);
        set.add(0);
        set.add(3);
        set.add(-2);

        assertEquals(Integer.valueOf(3), streamApiDemo.countOfPositiveNumbers.apply(set));
    }

    @Test
    public void testGetThreeLastElements(){
        StreamApiDemo streamApiDemo = new StreamApiDemo();

        Human human1 =new Human();
        Human human2 =new Human();
        Student student = new Student();
        Object object = new Object();

        List<Object> objects = new ArrayList<>();
        objects.add(human1);
        objects.add(human2);
        objects.add(student);
        objects.add(object);

        List<Object> listExpected = new ArrayList<>();
        listExpected.add(human2);
        listExpected.add(student);
        listExpected.add(object);

        List<Object> objectsBad = new ArrayList<>();
        objectsBad.add(student);
        objectsBad.add(object);

        assertEquals(listExpected, streamApiDemo.getThreeLastElements.apply(objects));
        assertEquals(null, streamApiDemo.getThreeLastElements.apply(objectsBad));
    }

    @Test
    public void testGetFirstEvenNumber(){
        StreamApiDemo streamApiDemo = new StreamApiDemo();

        List<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(-1);
        list.add(2);
        list.add(0);
        list.add(3);
        list.add(-2);

        assertEquals(Integer.valueOf(2), streamApiDemo.getFirstEvenNumber.apply(list));
    }

    @Test
    public void testGetListOfSquaresWithoutRepetitions(){
        StreamApiDemo streamApiDemo = new StreamApiDemo();

        Integer[] integers = {Integer.valueOf(1), Integer.valueOf(-1), Integer.valueOf(2), Integer.valueOf(-2), Integer.valueOf(3)};
        List<Integer> integersExpected = new ArrayList<>();
        integersExpected.add(Integer.valueOf(1));
        integersExpected.add(Integer.valueOf(4));
        integersExpected.add(Integer.valueOf(9));

        assertEquals(integersExpected, streamApiDemo.getListOfSquaresWithoutRepetitions.apply(integers));
    }

    @Test
    public void testGetListOfStringWithoutEmpty(){
        StreamApiDemo streamApiDemo = new StreamApiDemo();

        List<String> list = new ArrayList<>();
        list.add("c");
        list.add("");
        list.add("b");
        list.add("");
        list.add("a");

        List<String> listExpected = new ArrayList<>();
        listExpected.add("a");
        listExpected.add("b");
        listExpected.add("c");

        assertEquals(listExpected, streamApiDemo.getListOfStringWithoutEmpty.apply(list));
    }

    @Test
    public void testSetToListOfStrings(){
        StreamApiDemo streamApiDemo = new StreamApiDemo();

        Set<String> set = new HashSet<>();
        set.add("c");
        set.add("");
        set.add("b");
        set.add("");
        set.add("a");

        List<String> list = new ArrayList<>();
        list.add("c");
        list.add("");
        list.add("b");
        list.add("a");
        Collections.sort(list, new Comparator<String>() {
            @Override
            public int compare(String s1, String s2) {
                return s2.compareTo(s1);
            }
        });

        assertEquals(list, streamApiDemo.setToListOfStrings.apply(set));
    }

    @Test
    public void testGetSumOfSquares(){
        StreamApiDemo streamApiDemo = new StreamApiDemo();

        Set<Integer> set = new HashSet<>();
        set.add(1);
        set.add(-1);
        set.add(2);
        set.add(0);

        assertEquals(Integer.valueOf(6), streamApiDemo.getSumOfSquares.apply(set));
    }

    @Test
    public void testGetMaxAge(){
        StreamApiDemo streamApiDemo = new StreamApiDemo();

        Collection<Human> humans = new ArrayList<>();

        humans.add(new Human("", "", "", 12, ""));
        humans.add(new Human("", "", "", 24, ""));
        humans.add(new Human("", "", "", 36, ""));

        assertEquals(Integer.valueOf(36), streamApiDemo.getMaxAge.apply(humans));
    }

    @Test
    public void testSortHumansBySexAndAge(){
        StreamApiDemo streamApiDemo = new StreamApiDemo();

        Human human1 = new Human("", "", "", 12, "man");
        Human human2 = new Human("", "", "", 15, "woman");
        Human human3 = new Human("", "", "", 25, "man");
        Human human4 = new Human("", "", "", 23, "woman");

        Collection<Human> humans = new ArrayList<>();
        humans.add(human1);
        humans.add(human2);
        humans.add(human3);
        humans.add(human4);

        Collection<Human> humansExpected = new ArrayList<>();
        humansExpected.add(human1);
        humansExpected.add(human2);
        humansExpected.add(human3);
        humansExpected.add(human4);

        ((ArrayList<Human>) humansExpected).sort(new Comparator<Human>() {
            @Override
            public int compare(Human o1, Human o2) {
                if(o1.getSex().compareTo(o2.getSex()) == 0) {
                    return o1.getAge() - o2.getAge();
                }
                return o1.getSex().compareTo(o2.getSex());
            }
        });

        assertEquals(humansExpected, streamApiDemo.sortHumansBySexAndAge.apply(humans));
    }
}
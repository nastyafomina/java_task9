import org.junit.Test;
import static org.junit.Assert.*;

public class LambdaRunnerTest {
    @Test
    public void test(){
        LambdaRunner lambdaRunner = new LambdaRunner();
        assertEquals(Integer.valueOf(5), lambdaRunner.useFunctionString(String::length, "abcde"));
        assertEquals(Integer.valueOf(3), lambdaRunner.useFunctionString((String s) -> {return s.split(",").length; }, "ab,c,de"));
        assertEquals(Character.valueOf('a'), lambdaRunner.useFunctionString((String s) -> s.charAt(0), "abcde"));
        assertEquals(Boolean.valueOf(true), lambdaRunner.usePredicate((String s) -> s.contains(" "), "abcde"));
        assertEquals(Boolean.valueOf(false), lambdaRunner.usePredicate((String s) -> s.contains(" "), "ab c de"));
        assertEquals(Integer.valueOf(42), LambdaRunner.useFunctionHuman((Human human) -> human.getAge(), new Human("", "", "", 42, "")));
        assertTrue(LambdaRunner.useBiPredicate((Human human1, Human human2) -> human1.getSurname().equals(human2.getSurname()),
                new Human("Ivanov", "", "", 0, ""), new Human("Ivanov", "", "", 0, "")));
        assertFalse(LambdaRunner.useBiPredicate((Human human1, Human human2) -> human1.getSurname().equals(human2.getSurname()),
                new Human("Ivanov", "", "", 0, ""), new Human("Petrov", "", "", 0, "")));
        assertEquals("Ivanov Ivan Ivanovich", lambdaRunner.useFunctionHuman((Human human) -> human.getSurname()+" "+human.getName()+" "+human.getPatronymic(),
                new Human("Ivanov", "Ivan", "Ivanovich", 42, "")));
        assertEquals(43, (LambdaRunner.useUnaryOperator((Human human) -> new Human(human.getSurname(), human.getName(), human.getPatronymic(), human.getAge()+1, human.getSex()),
                new Human("Ivsnov", "Ivan", "Ivanovich", 42, "")).getAge()));
        assertTrue(lambdaRunner.useICheckAge((Human human1, Human human2, Human human3, int maxAge) -> human1.getAge()<maxAge && human2.getAge()<maxAge
                        && human3.getAge()<maxAge, new Human("", "", "", 22, ""),
                new Human("", "", "", 33, ""), new Human("", "", "", 44, ""), 55));
        assertFalse(lambdaRunner.useICheckAge((Human human1, Human human2, Human human3, int maxAge) -> human1.getAge()<maxAge && human2.getAge()<maxAge
                        && human3.getAge()<maxAge, new Human("", "", "", 22, ""),
                new Human("", "", "", 33, ""), new Human("", "", "", 44, ""), 11));
    }
}